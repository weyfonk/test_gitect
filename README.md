## Why?

This project is used by the [gitect](https://gitlab.com/trustable/gitect) CI to test evidence manipulation.

It should not be used to store any important data.
